# Very Simple Audio Player

Can load a single audio file and allow the user to play, pause, resume or seek through the audio. Powered by FFMpeg and wxPython.

## To-do:
- Properly close the program
- Make the seek position-updating thread more reactive
- Make that process thread-safe (I have no idea how)
- Add a playlist
- Implement repeat, reapeat-one, random and shuffle functions
- Build a music library
- Add the ability to create multiple playlists