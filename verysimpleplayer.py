import wx, ffpyplayer.player as ffplay
from threading import Thread
import time

class VerySimplePanel(wx.Panel):
    def __init__(self, parent):
        super().__init__(parent)

        self.SEEK_UPDATE_THREAD = Thread(target=self.UpdateSeekPosition)
        self.SEEK_MAXVALUE = 1000
        self.VOLUME_MAXVALUE = 200

        self.filename = ""
        self.filelength = 0.0
        self.fileobject = None
        self.volume = 1.0

        self.fileLabel = wx.StaticText(self, label="No file loaded.")
        playButton = wx.Button(self, label="Play", size=(-1,-1))
        pauseButton = wx.Button(self, label="Pause", size=(-1,-1))
        stopButton = wx.Button(self, label="Stop", size=(-1,-1))
        volumeSlider = wx.Slider(self, size=(120, -1), maxValue=self.VOLUME_MAXVALUE, value=self.VOLUME_MAXVALUE)
        self.seekSlider = wx.Slider(self, maxValue=self.SEEK_MAXVALUE)

        self.Bind(wx.EVT_BUTTON, self.OnPlay, playButton)
        self.Bind(wx.EVT_BUTTON, self.OnPause, pauseButton)
        self.Bind(wx.EVT_BUTTON, self.OnStop, stopButton)
        self.Bind(wx.EVT_SCROLL, self.OnVolumeChange, volumeSlider)
        self.Bind(wx.EVT_SCROLL_THUMBRELEASE, self.OnSeekChange, self.seekSlider)
        self.Bind(wx.EVT_SCROLL_PAGEUP, self.OnSeekChange, self.seekSlider)
        self.Bind(wx.EVT_SCROLL_PAGEDOWN, self.OnSeekChange, self.seekSlider)

        labelSizer = wx.BoxSizer(wx.HORIZONTAL)
        labelSizer.Add(self.fileLabel, 0, wx.CENTER, 5)

        controls = wx.BoxSizer(wx.HORIZONTAL)
        controls.Add(playButton, 0, wx.CENTER | wx.ALL, 5)
        controls.Add(pauseButton, 0, wx.CENTER | wx.ALL, 5)
        controls.Add(stopButton, 0, wx.CENTER | wx.ALL, 5)
        controls.Add(volumeSlider, 0, wx.CENTER | wx.ALL, 5)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(labelSizer, 1, wx.CENTER, 5)
        sizer.Add(self.seekSlider, 0, wx.EXPAND, 5)
        sizer.Add(controls, 0, wx.CENTER, 5)
        self.SetSizer(sizer)

    def OnPlay(self, event):
        if self.fileobject:
            self.fileobject.set_volume(self.volume)
            self.fileobject.set_pause(False)

    def OnPause(self, event):
        if self.fileobject:
            self.fileobject.set_pause(True)

    def OnStop(self, event):
        if self.fileobject:
            self.fileobject.set_pause(True)
            self.fileobject.seek(0.0, relative=False)

    def OnVolumeChange(self, event):
        #print(f"DEBUG Volume: {event.GetPosition()}")
        self.volume = event.GetPosition()/float(self.VOLUME_MAXVALUE)
        if self.fileobject:
            self.fileobject.set_volume(self.volume)

    def OnSeekChange(self, event):
        if self.fileobject:
            position = self.filelength * event.GetPosition() / float(self.SEEK_MAXVALUE)
            self.fileobject.seek(position, relative=False) # Not thread safe, NOT GOOD!
            #print(f"DEBUG Seek: {event.GetPosition()}, {position}")

    def LoadFile(self, filename):
        #print(f"DEBUG Thread: {self.SEEK_UPDATE_THREAD.is_alive()}")
        if self.fileobject:
            self.fileobject.close_player()

        self.filename = filename
        self.fileobject = ffplay.MediaPlayer(filename, callback=self.PlayerEOF,
            ff_opts={'paused': True, 'vn': True, 'sn': True})

        self.fileLabel.SetLabel(filename)
        self.filelength = 0.0
        t = Thread(target=self.WaitForMetadata)
        t.start()
        t.join()
        self.filelength = self.fileobject.get_metadata()['duration']

        if not self.SEEK_UPDATE_THREAD.is_alive():
            self.SEEK_UPDATE_THREAD.start()
        #print(f"DEBUG Metadata: {self.filelength}")

    def PlayerEOF(self, selector, value):
        #print(f"DEBUG callback({selector}, {value})")
        if selector == 'eof':
            self.fileobject.set_pause(True)
            self.fileobject.seek(0.0, relative=True)

    def WaitForMetadata(self):
        while True:
            if self.fileobject.get_metadata()['duration']:
                self.filelength = self.fileobject.get_metadata()['duration']
                return

    def UpdateSeekPosition(self):
        while True:
            position = int(self.SEEK_MAXVALUE * self.fileobject.get_pts()/self.filelength)
            self.seekSlider.SetValue(position) # Not thread safe, though it's okay for this to change
            time.sleep(1)

    def OnClose(self):
        if self.fileobject:
            self.SEEK_UPDATE_THREAD.join()
            self.fileobject.close_player()

class VerySimpleFrame(wx.Frame):
    def __init__(self):
        super().__init__(None, title="Very Simple Music Player")

        self.panel = VerySimplePanel(self)
        self.MakeMenuBar()
        self.CreateStatusBar() # Purely for aesthetic reasons
        self.Show()

    def MakeMenuBar(self):
        fileMenu = wx.Menu()
        openFileItem = fileMenu.Append(wx.ID_ANY, '&Open File...\tCtrl-O',
            'Open a sound file')
        fileMenu.AppendSeparator()
        exitFileItem = fileMenu.Append(wx.ID_EXIT)

        self.Bind(wx.EVT_MENU, self.OnOpenFile, openFileItem)
        self.Bind(wx.EVT_MENU, self.OnExit, exitFileItem)

        menuBar = wx.MenuBar()
        menuBar.Append(fileMenu, '&File')
        self.SetMenuBar(menuBar)

    def OnOpenFile(self, event):
        with wx.FileDialog(self, "Open file(s)...", style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST,
            wildcard="All Files|*") as fileDialog:
            if fileDialog.ShowModal() == wx.ID_OK:
                file = fileDialog.GetPath()
                self.panel.LoadFile(file)

    def OnExit(self, event):
        if self.panel.fileobject:
            self.panel.fileobject.close_player()
        self.Close()

if __name__ == '__main__':
    app = wx.App()
    frame = VerySimpleFrame()
    app.MainLoop()
